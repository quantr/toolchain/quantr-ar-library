package hk.quantr.ar;

import java.io.File;
import java.util.ArrayList;
import org.junit.Test;
import java.util.logging.Logger;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Test1 {

	public static final Logger logger = Logger.getLogger(Test1.class.getName());

	@Test
	public void fullTest() throws Exception {
		QuantrAR peterAR = new QuantrAR();
		ArrayList<AR> data = peterAR.init(new File(Test1.class.getClassLoader().getResource("libLLVMMipsDesc.a").getPath()));
		if (data == null) {
			System.err.println("read error");
		} else {
			for (AR ar : data) {
				System.out.println("ar=" + ar);
			}
		}
	}
}
