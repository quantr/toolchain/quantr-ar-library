package hk.quantr.ar;

public class AR {

	public String filename;
	public String tempstamp;
	public String ownerID;
	public String groupID;
	public String fileMode;
	public int fileSize;
	public byte bytes[];

	@Override
	public String toString() {
		return "filename: " + filename + ",\ttempstamp: " + tempstamp + ",\townerID: " + ownerID + ", groupID: " + groupID + ", fileMode: " + fileMode + ", fileSize: " + fileSize;
	}
}

/**
 * AR2 is a class
 * 
 * @author Peter <peter@quantr.hk>;
 */
class AR2 {

	public static int func1(String s) {
		return 1;
	}
}
