# Introduction

A java library to read the file that is unix archive format. A big Elf file is using that format to combose many different elf files. My blog is : http://peter.quantr.hk

# Author

Peter, peter@quantr.hk

# Example

```
QuantrAR peterAR = new QuantrAR();
Vector<AR> data = peterAR.init(new File("your file"));
for (AR ar: data){
    System.out.println(ar);
}
```
